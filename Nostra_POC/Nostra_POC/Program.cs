﻿using Amazon;
using Amazon.SQS;
using Amazon.SQS.Model;
using System;
using System.Configuration;
using System.Threading.Tasks;

namespace Nostra_POC
{
    class Program
    {
        private static string _SQSServer { get; set; }
        private static string _TPASCorrelationId { get; set; }

        private static AmazonSQSClient SQSClient()
        {
            return new AmazonSQSClient(ConfigurationManager.AppSettings["SQSApiKey"], ConfigurationManager.AppSettings["SQSApiSecret"], RegionEndpoint.EUWest1);
        }
        private static async Task SendMessage()
        {
            Console.WriteLine("SendMessage-------\n");
            try
            {
                using (var sqsClient = SQSClient())
                {
                    _TPASCorrelationId = $"tpas-{Guid.NewGuid()}";
                    var sendResponse = await sqsClient.SendMessageAsync(new SendMessageRequest
                    {
                        QueueUrl = $"{_SQSServer}{ConfigurationManager.AppSettings["SQSOutbound"]}",
                        MessageGroupId = "BrolinkTestGroup",
                        MessageDeduplicationId = Guid.NewGuid().ToString(),
                        MessageBody = $"{{\"originalMessageText\":\"\",\"apiVersion\":\"0.1\",\"messageType\":\"UpdateSubmissionRequest\",\"tpasCorrelationId\":\"{_TPASCorrelationId}\",\"insurerCorrelationId\":\"\",\"insurerPolicyNumber\":\"92800346031\",\"firstName\":\"Piet123\"}}"
                    });
                    Console.WriteLine($"SentMessage Statuscode: {sendResponse.HttpStatusCode}\n\n");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"SendMessage Exception: {ex.Message}\n\n");
            }
        }

        private static async Task ReceiveMessages()
        {
            Console.WriteLine("ReceiveMessages-------\n\n");
            try
            {
                using (var sqsClient = SQSClient())
                {
                    var queueUrl = $"{_SQSServer}{ConfigurationManager.AppSettings["SQSInbound"]}";
                    bool messagesFound = false;
                    while (!messagesFound)
                    {
                        Console.WriteLine("Checking for messages----------\n");
                        var receiveResponse = await sqsClient.ReceiveMessageAsync(new ReceiveMessageRequest
                        {
                            QueueUrl = queueUrl,
                            VisibilityTimeout = int.Parse(ConfigurationManager.AppSettings["MessageVisibilityTimeout"]),
                            WaitTimeSeconds = int.Parse(ConfigurationManager.AppSettings["MessageWaitTimeSeconds"])
                        });

                        if (receiveResponse.Messages.Count > 0)
                            messagesFound = true;

                        foreach (var message in receiveResponse.Messages)
                        {
                            Console.WriteLine("Reading message----------");
                            Console.WriteLine($"MessageId:{message.MessageId}\nMessageBody:{message.Body}\nReceiptHandle:{message.ReceiptHandle}");
                            Console.WriteLine("END----------\n\n");
                            Console.WriteLine("Delete message----------");
                            var deleteResponse = await sqsClient.DeleteMessageAsync(new DeleteMessageRequest(queueUrl, message.ReceiptHandle));
                            Console.WriteLine($"DeleteMessage Statuscode: {deleteResponse.HttpStatusCode}");
                            Console.WriteLine("END----------\n\n");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"ReceiveMessages Exception: {ex.Message}");
            }
        }
        static async Task Main(string[] args)
        {
            Console.WriteLine("Starting AWS SQS----------\n\n");

            _SQSServer = ConfigurationManager.AppSettings["AWSServer"];

            await SendMessage();

            await ReceiveMessages();

            Console.WriteLine("Ending AWS SQS----------\n\n");
            Console.ReadLine();
        }
    }
}
